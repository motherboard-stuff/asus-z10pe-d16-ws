import sys

if len(sys.argv) != 4:
    print("Usage:\n\tpython3 capify.py extracted_cap_header bios_filename new_cap_output_filename")
    print("Example:\n\tpython3 capify.py Z10PE-D16-WS-ASUS-4101.hdr Z10PE-D16-WS-ASUS-modified.bin Z1016WS.CAP")
    sys.exit(0)

cap_header_filename = sys.argv[1]
print("Opening CAP header file:", cap_header_filename)
with open(cap_header_filename, 'rb') as f:
    header = f.read()

bios_filename = sys.argv[2]
print("Opening BIOS image file:", bios_filename)
with open(bios_filename, 'rb') as f:
    bios = f.read()

print("Joining BIOS image with CAP header.")
newcap = header + bios

cap_filename = sys.argv[3]
print("Writing new CAP file:", cap_filename)
with open(cap_filename, 'wb') as f:
    f.write(newcap)

