import sys

if len(sys.argv) != 4:
    print("Usage:\n\tpython3 decapify.py cap_filename extracted_cap_header extracted_bios_filename")
    print("Example:\n\tpython3 decapify.py Z10PE-D16-WS-ASUS-4101.CAP Z10PE-D16-WS-ASUS-4101.hdr Z10PE-D16-WS-ASUS-4101.bin")
    sys.exit(0)

print("Reading CAP file:", sys.argv[1])
with open(sys.argv[1], 'rb') as f:
    cap = f.read()

print("Extracting CAP header from CAP file.")
header = cap[0:2048]

print("Extracting BIOS image from CAP file.")
bios = cap[2048:]

print("Writing extracted CAP header file:", sys.argv[2])
with open(sys.argv[2], 'wb') as f:
    f.write(header)

print("Writing extracted BIOS image file:", sys.argv[3])
with open(sys.argv[3], 'wb') as f:
    f.write(bios)

